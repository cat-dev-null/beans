--[[
require("List")

exf = {}

function love.load()
    exf.list = List:new()
    exf.smallfont = love.graphics.newFont(12)
    exf.bigfont = love.graphics.newFont(24)
    exf.list.font = exf.bigfont
  

    
    exf.list:add("Local Game")
    exf.list:add("Network Game")
    exf.list:add("Quit")
    
    exf.list:done()
end


function love.draw()
    love.graphics.setBackgroundColor(54, 172, 248)

    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(exf.bigfont)
    love.graphics.print("Bohnanza", 50, 50)
    love.graphics.print("Usage", 500, 50)

    love.graphics.setFont(exf.smallfont)
    love.graphics.print("Browse and click on the example you \nwant to run. To return the the example \nselection screen, press escape.\n\nIf you would like to add an \nexample, please contact us! ^.^)/", 500, 80)


    exf.bigball = love.graphics.newImage("images/love-big-ball.png")
    love.graphics.draw(exf.bigball, 800 - 128, 600 - 128)
    exf.list:draw()
end
--]]
CardType = {}
CardType.__index = CardType

function CardType:new(name, new_count, new_coins)
    --Have no clue what this stuff does
    o = {}
    setmetatable(o, self)
  
    o.name = name
    o.coins = {}
    o.count = new_count;
    for i=1, 3 do
      o.coins[i] = new_coins[i];
    end
    return o
end

function CardType:harvest(self, harvest_count)
  for i=1, 3 do
    if harvest_count < self.coins[i] then
      return i-1
    end
  end
  return 4
end
Field = {}
Field.__index = Field
function Field:new()
  o = {}
  setmetatable(o, self)
  o.type = nil
  o.count = 0
  return o
end